# libraries

import numpy as np
from influxdb import InfluxDBClient
import datetime as dt
import pandas as pd
import time
import os
import math
import json
import datetime

def current_settings():
    # Query db and get csv
    query = 'select * from settings_data' 
    client = InfluxDBClient('localhost', 8086, 'jared', 'qwertyui', 'settings_db')
    result = client.query(query)
    n4l_agri = list(result.get_points('settings_data')) 
    df_n4l_agri = pd.DataFrame(n4l_agri)
    df_n4l_agri = pd.DataFrame(n4l_agri)
    if df_n4l_agri.size:
        df_n4l_agri[['time']] = df_n4l_agri[['time']].apply(pd.to_datetime)
        # sorting based on time
        df_n4l_agri.sort_values(by='time');
    df_n4l_agri = pd.concat([df_n4l_agri], sort=False)
    #change time zone
    df_n4l_agri['time'] = pd.DatetimeIndex(df_n4l_agri['time']) + dt.timedelta(hours=3)
    df_n4l_agri = df_n4l_agri.reset_index(drop=True)
    df_n4l_agri = df_n4l_agri.dropna()
    water_min = df_n4l_agri.iloc[-1]['water_vol_min']
    water_max = df_n4l_agri.iloc[-1]['water_vol_max']
    return water_min, water_max

if __name__ == "__main__":
    current_settings()
