
# libraries

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_auth
import numpy as np
from influxdb import InfluxDBClient
import datetime as dt
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
import time
import os
import math
import json
import dash_table
from itertools import zip_longest
import dash_bootstrap_components as dbc
import datetime as dt
import datetime

db_client = InfluxDBClient(host='localhost', port=8086) 
db_client.create_database('settings_db') # create settings db

# Keep this out of source code repository - save in a file or a database
VALID_USERNAME_PASSWORD_PAIRS = {
    'qwertyui': '123qwertyui'
}
token = open("./static/my_token.csv").read() # you need your own token

# refresh frequency

UPDATE_INTERVAL = 15 * 60 * 1000 # 15 minutes
DATE_RANGE_UPDATE = 30 * 60 * 1000 # 30 minutes
UPDATE_INTERVAL_1 = os.environ.get("UPDATE_INTERVAL_1", 1 * 60 * 1000 )

# Querry durations

DAY_QUERY = '1d'
LONG_QUERY = '30d'
previous_n_clicks = 0

# colors

colors = {
    'text': '#000000',
    'background': '#C0C0C0',
    'button': '#FF8C00',
    'button-text': '#ffffff',
    'graph': '#8b6914',
}

user_em = 'jaredmaks@gmail.com'

# Mapbox token

def user_info():
    df_users = pd.read_csv('./static/users_inv.csv') # users details
    is_user_spec =  df_users['user_email']== user_em
    df_user = df_users[is_user_spec]
    df_options= df_user[['greenhouse', 'device_eui']]
    return df_options


# Convertion to dictionary

def dev_per_greenh():
    all_options = {}
    for i in user_info()["greenhouse"].tolist(): 
        all_options[i] = user_info()[user_info()["greenhouse"]==i]["device_eui"].tolist() 
    return all_options

new_parameters = {
    'env_sensor': ['amb_temp', 'amb_humi'],
    'light_sensor': ['light'],
    'soil_sensor': ['soil_ec', 'soil_temp', 'soil_moisture'],
    'ph_sensor': ['soil_pH'],
    'npk_sensor': ['nitrogen', 'phosphorus', 'potassium'],
    'water_meter': ['water_vol'],
    'micro_tracker': ['location']
}

def generate_table(dataframe, max_rows=10):
    return dash_table.DataTable(
        data=dataframe.to_dict('records'),
        columns=[{'id': c, 'name': c} for c in dataframe.columns], style_header={'backgroundColor': 'rgb(30, 30, 30)'},
        style_cell={
            'backgroundColor': 'rgb(50, 50, 50)',
            'color': 'white',
            'whiteSpace': 'normal',
            'height': 'auto'
    },
)

# settings db query

# database querry

def query_db(past_duration):
    query = 'select * from agri_data where time > now() - 4w' + past_duration
    client = InfluxDBClient('15.188.249.228', 8086, 'n4l', 'qwertyui123', 'n4l_agri_db')
    result = client.query(query)
    result_list_gh = list(result.get_points('agri_data')) 
    df_gh = pd.DataFrame(result_list_gh)
    df_gh = pd.DataFrame(result_list_gh)
    if df_gh.size:
        df_gh[['time']] = df_gh[['time']].apply(pd.to_datetime)
        df_gh.sort_values(by='time')
    df_gh = pd.concat([df_gh], sort=False)
    df_gh['time'] = pd.DatetimeIndex(df_gh['time']) + dt.timedelta(hours=0)
    df_gh = df_gh.reset_index(drop=True)        
    return df_gh

def group(n, iterable, fillvalue=None): 
    args = [iter(iterable)] * n 
    return zip_longest(fillvalue=fillvalue, *args)

def device_map_update(end_point):
    str = end_point
    n=2
    out_string = [''.join(lis) for lis in group(n, str, '')]  
    out_no = [] 

    for a in out_string: 
        out_no.append(a)

    if out_no[0] == '00' and out_no[1] == '13':
        new_params = new_parameters['soil_sensor']
        return new_params
    elif out_no[0] == '00' and out_no[1] == '14':
        new_params = new_parameters['light_sensor']
        return new_params
    elif out_no[0] == '00' and out_no[1] == '15':
        new_params = new_parameters['env_sensor']
        return new_params
    elif out_no[0] == '00' and out_no[1] == '16':
        new_params  = new_parameters['ph_sensor']
        return new_params
    elif out_no[0] == '00' and out_no[1] == '17':
        new_params = new_parameters['npk_sensor']
        return new_params
    elif out_no[0] == '70' and out_no[1] == 'B3':
        new_params = new_parameters['water_meter']
        return new_params
    else:
        print('no device')


FA = "https://use.fontawesome.com/releases/v5.15.1/css/all.css"

app = dash.Dash(external_stylesheets=[dbc.themes.SUPERHERO, FA], suppress_callback_exceptions=True, title="N4L_Agri_App",
                 assets_folder ="static",
                 assets_url_path="static")
application = app.server
sidebar = html.Div(
    [
        html.Div(
            [
                # width: 3rem ensures the logo is the exact width of the
                # collapsed sidebar (accounting for padding)
                html.Img(src='./static/logo.jpg', style={"width": "3rem"}),
                html.H4("N4LA AGRI APP"),
            ],
            className="sidebar-header",
        ),
        html.Hr(),
        dbc.Nav(
            [
                dbc.NavLink(
                    [
                        html.I(className="fas fa-tachometer-alt mr-2"), 
                        html.Span("Dashboard")],
                    href="/dashboard",
                    active="dashboard",
                ),
                dbc.NavLink(
                    [
                        html.I(className="fas fa-cogs mr-2"),
                        html.Span("Settings"),
                    ],
                    href="/settings",
                    active="exact",
                ),
                dbc.NavLink(
                    [
                        html.I(className="fas fa-info-circle mr-2"),
                        html.Span("Data Frames"),
                    ],
                    href="/dataframes",
                    active="exact",
                ),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    className="sidebar",
)

content = html.Div(id="page-content", className="content")

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])

# callbacks

@app.callback(
    Output('device_dropdown', 'options'),
    [Input('green_dropdown', 'value')])
def greenhouse_update(greenh):
    return [{'label': i, 'value': i} for i in dev_per_greenh()[greenh]]

@app.callback(
    Output('device_dropdown', 'value'),
    Input('device_dropdown', 'options'))
def devices_update(devices_opt):
    return devices_opt[0]['value']

@app.callback(
    Output('parameter_dropdown', 'options'),
    Input('device_dropdown', 'value'))
def device_parameter_update(devices_opt):
    str = devices_opt
    n=2
    out_string = [''.join(lis) for lis in group(n, str, '')]  
    out_no = [] 

    for a in out_string: 
        out_no.append(a)

    if out_no[0] == '00' and out_no[1] == '13':
        return [{'label': j, 'value': j} for j in new_parameters['soil_sensor']]
    elif out_no[0] == '00' and out_no[1] == '14':
        return [{'label': j, 'value': j} for j in new_parameters['light_sensor']]
    elif out_no[0] == '00' and out_no[1] == '15':
        return [{'label': j, 'value': j} for j in new_parameters['env_sensor']]
    elif out_no[0] == '00' and out_no[1] == '16':
        return [{'label': j, 'value': j} for j in new_parameters['ph_sensor']]
    elif out_no[0] == '00' and out_no[1] == '17':
        return [{'label': j, 'value': j} for j in new_parameters['npk_sensor']]
    elif out_no[0] == '70' and out_no[1] == 'B3':
        return [{'label': j, 'value': j} for j in new_parameters['water_meter']]
    else:
        print('no device')
        
@app.callback(
    Output('parameter_dropdown', 'value'),
    Input('parameter_dropdown', 'options'))
def parameter_update(devices_opt):
    return devices_opt[0]['value']

@app.callback(
    Output('map', 'figure'),
    Output('plot', 'figure'),
    Input('green_dropdown', 'value'),
    Input('device_dropdown', 'value'),
    Input('parameter_dropdown', 'value'),
    Input('date-range', 'start_date'),
    Input('date-range', 'end_date'))
def update_dropdowns(greenh, devices_opt, parameter, start_date, end_date):
    df_all = query_db(LONG_QUERY)
    df = df_all[df_all['sensor'] == devices_opt] 

    # get all devices on selected greenhouse

    start_date = dt.datetime.strptime(start_date, '%Y-%m-%d')
    end_date = dt.datetime.strptime(end_date, '%Y-%m-%d')
    filtered_df = df[(df['time'].dt.date>=start_date.date()) &
                     (df['time'].dt.date<=end_date.date())]
    df_plot = filtered_df[['time', 'lat', 'lon', 'sensor', parameter]]
    df_plot = df_plot.reset_index(drop=True)

    # Create traces for parameters

    fig1 = go.Figure()
    fig1.add_trace(go.Scatter(x=df_plot.time, y=df_plot[parameter],
                        mode='lines+markers',
                        name='lines+markers'))
    fig1.update_layout(xaxis_title="Date", yaxis_title= parameter, margin=dict(l=0, r=5, t=5, b=0), paper_bgcolor="LightSteelBlue", font=dict(
        family="Courier New, monospace",
        size=12,
        color="#000000"
    ))


    df_greenhspec = user_info()[user_info()['greenhouse'] == greenh]
    device_list = df_greenhspec['device_eui'].tolist()

    device_list = df_greenhspec['device_eui'].tolist()
    new_greenhouse_df = df_all[df_all['dev_eui'].isin(device_list)]
    new_greenhouse_df = new_greenhouse_df.reset_index(drop=True)
    # get alldevices in a specific greenhouse
    df_k = df_all[df_all['sensor'].isin(device_list)]
    df_k = df_k.groupby('sensor').tail(1)
    df_k['greenhouse'] = greenh
    df_k = df_k.reset_index(drop=True)

    fig = px.scatter_mapbox(df_k, lat='lat', lon='lon', hover_name='greenhouse', size='frame_counter', 
                            hover_data= ['sensor','water_vol', 'soil_ec','soil_temp','soil_moisture',
                                        'nitrogen','light','phosphorus', 'potassium', 'amb_humi', 'amb_temp','soil_pH'],
                        zoom=9, height=400)
    fig.update_layout(mapbox_style="satellite")

    fig.update_layout(
        mapbox = {
            'accesstoken': token,
            'style': "satellite", 'zoom': 16},
        margin=dict(t=0, b=0, l=0, r=0),
        showlegend = False)
    fig.update_layout(
        autosize=True,
        hovermode='closest',
        showlegend=False,
        mapbox=dict(
            accesstoken=token,
            bearing=0,
            center=dict(
                lat=-1.290355310854171,
                lon=36.76491022109985
            ),
            pitch=0,
        ),
    )

    fig.data[0].update(hovertemplate='<b>%{hovertext}</b><br><br>frame_counter=%{marker.size}<br>sensor=%{customdata[0]}<br>')
    return fig, fig1

# glance elements update

@app.callback(
    Output('glance', 'children'),
    Input('device_dropdown', 'value'),
    Input('date-range', 'start_date'),
    Input('date-range', 'end_date'))
def glance_update(devices_opt, start_date, end_date):
    df_all = query_db(LONG_QUERY)
    df = df_all[df_all['sensor'] == devices_opt] 

    start_date = dt.datetime.strptime(start_date, '%Y-%m-%d')
    end_date = dt.datetime.strptime(end_date, '%Y-%m-%d')
    filtered_df = df[(df['time'].dt.date>=start_date.date()) &
                     (df['time'].dt.date<=end_date.date())]

    # glance elements
    new_params_list = device_map_update(devices_opt)
    glance_list = ['time', 'sensor']
    for y in new_params_list:
        glance_list.append(y)
    
    df_glance = filtered_df[glance_list]
    df_final = df_glance.iloc[-1]
    df_final = df_final.reset_index(drop=True)

    str = devices_opt
    n=2
    out_string = [''.join(lis) for lis in group(n, str, '')]  
    out_no = [] 

    for a in out_string: 
        out_no.append(a)
    
    # universal time track for glance realtime elements

    time_now = filtered_df[glance_list[0]].iloc[-1].date()
    time_latest = filtered_df[glance_list[0]].iloc[-1].time().replace(microsecond = 0)

    
    # Soil sensor

    if out_no[0] == '00' and out_no[1] == '13':
        soil_ec = filtered_df[glance_list[2]].iloc[-1] 
        soil_temp = filtered_df[glance_list[3]].iloc[-1]
        soil_moist = filtered_df[glance_list[4]].iloc[-1]
        
        return html.Div(
            [
                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Soil EC")),
                        dbc.CardImg(src="./static/Soil_EC.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} S/m'.format(soil_ec), lg=3),
                ], align="center"),

                html.Hr(),

                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Soil Temperature")),
                        dbc.CardImg(src="./static/Soil_Temp.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} °C'.format(soil_temp), lg=3),
                ], align="center"),

                html.Hr(),
                
                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Soil Moisture")),
                        dbc.CardImg(src="./static/Soil_Moisture.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} %'.format(soil_moist), lg=3),
                ], align="center"),           
            ])

    # light sensor        

    elif out_no[0] == '00' and out_no[1] == '14':
        light_level = filtered_df[glance_list[2]].iloc[-1]
        return html.Div(
            [
                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Light Intensity")),
                        dbc.CardImg(src="./static/Light_Intensity.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} lux'.format(light_level), lg=3),
                ], align="center"),         
            ])

    # Environmental sensor        

    elif out_no[0] == '00' and out_no[1] == '15':
        amb_temp = filtered_df[glance_list[2]].iloc[-1]
        amb_humi = filtered_df[glance_list[3]].iloc[-1]
        return html.Div(
            [
                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Temperature")),
                        dbc.CardImg(src="./static/Ambient_Temp.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} °C'.format(amb_temp), lg=3),
                ], align="center"), 

                html.Hr(),

                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Humidity")),
                        dbc.CardImg(src="./static/Ambient_Humid.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} %'.format(amb_humi), lg=3),
                ], align="center"),        
            ])

    # Soil pH sensor        

    elif out_no[0] == '00' and out_no[1] == '16':
        soil_pH = filtered_df[glance_list[2]].iloc[-1]
        return html.Div(
            [
                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Soil pH")),
                        dbc.CardImg(src="./static/Soil_pH.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{}'.format(soil_pH), lg=3),
                ], align="center"),         
            ])

    # Water meter endpoint       

    elif out_no[0] == '70' and out_no[1] == 'B3':
        water_vol = filtered_df[glance_list[2]].iloc[-1]
        return html.Div(
            [
                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Water Used")),
                        dbc.CardImg(src="./static/Smart_Meter.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} Liters'.format(water_vol), lg=3),
                ], align="center"),         
            ])

    # NPK sensor        

    elif out_no[0] == '00' and out_no[1] == '17':
        nitrogen = filtered_df[glance_list[2]].iloc[-1]
        phosphorus = filtered_df[glance_list[3]].iloc[-1]
        potassium = filtered_df[glance_list[4]].iloc[-1]
        return html.Div(
            [
                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Nitrogen")),
                        dbc.CardImg(src="./static/Soil_Nitr.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} %'.format(nitrogen), lg=3),
                ], align="center"),

                html.Hr(),

                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Phosphorus")),
                        dbc.CardImg(src="./static/Soil_Phos.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} %'.format(phosphorus), lg=3),
                ], align="center"),

                html.Hr(),

                dbc.Row([
                    dbc.Col([
                        dbc.Col(html.Div("Potassium")),
                        dbc.CardImg(src="./static/Soil_Pota.png")], lg=4),
                    dbc.Col([
                        dbc.Row(time_now, justify='center'),
                        dbc.Row(time_latest , justify='center'),
                        ], lg=5),
                    dbc.Col('{} %'.format(potassium), lg=3),
                ], align="center"),           
            ])
    else:
        print('no device')


@app.callback(
    Output('analysis', 'children'),
    Input('device_dropdown', 'value'),
    Input('date-range', 'start_date'),
    Input('date-range', 'end_date'))
def analysis_update(devices_opt, start_date, end_date):
    df_all = query_db(LONG_QUERY)
    df_sensor = df_all[df_all['sensor'] == devices_opt] 

    start_date = dt.datetime.strptime(start_date, '%Y-%m-%d')
    end_date = dt.datetime.strptime(end_date, '%Y-%m-%d')
    filtered_df = df_sensor[(df_sensor['time'].dt.date>=start_date.date()) &
                     (df_sensor['time'].dt.date<=end_date.date())]

    # check if device is water meter

    new_params_list = device_map_update(devices_opt)
    glance_list = ['time', 'sensor']
    for y in new_params_list:
        glance_list.append(y)
    
    df_glance = filtered_df[glance_list]
    df_final = df_glance.iloc[-1]
    df_final = df_final.reset_index(drop=True)

    str = devices_opt
    n=2
    out_string = [''.join(lis) for lis in group(n, str, '')]  
    out_no = [] 

    for a in out_string: 
        out_no.append(a)


    if out_no[0] == '70' and out_no[1] == 'B3':

        # Water data analysis daily and monthly
        
        df_water = filtered_df[['time', 'sensor','water_vol']]
        df_water.rename(columns = {'time':'Date'}, inplace = True)
        df_clean = df_water[df_water['water_vol'].notna()]
        df = df_clean[df_clean['sensor'] == devices_opt]
        df = df.reset_index(drop=True)
        df['Consuption (Liters)'] = df['water_vol'].diff().fillna(df['water_vol'])
        df = df[1:]

        # df = df.set_index('time')
        df = df.set_index(pd.DatetimeIndex(df['Date']))
        df = df.drop(['Date'], axis=1)

        # daily
        df_daily = df.resample('D').sum()

        # montly resampling
        df_monthly = df.resample('M').sum()

        # daily consuption
        fig_daily = px.bar(df_daily, x=df_daily.index, y='Consuption (Liters)')
        fig_daily.update_traces(textposition='outside')
        fig_daily.update_layout(uniformtext_minsize=8, uniformtext_mode='hide', margin=dict(l=20, r=20, t=20, b=20), paper_bgcolor="LightSteelBlue")

        # monthly consuption
        fig_monthly = px.bar(df_monthly, x=df_monthly.index, y='Consuption (Liters)')
        fig_monthly.update_traces(textposition='outside')
        fig_monthly.update_layout(uniformtext_minsize=8, uniformtext_mode='hide', margin=dict(l=20, r=20, t=20, b=20), paper_bgcolor="LightSteelBlue")
    
        return html.Div([
            dbc.Row([
                dbc.Col([
                    dbc.Row(dbc.Col(html.Div("Daily Consuption"))),
                    dcc.Graph(
                        id='daily-graph',
                        figure=fig_daily,
                        config= {'displaylogo': False}
                )], width=6),
                dbc.Col([
                    dbc.Row(dbc.Col(html.Div("Monthly Consuption"))),
                    dcc.Graph(
                        id='monthly-graph',
                        figure=fig_monthly,
                        config= {'displaylogo': False}
                )], width=6),

            ])
        ])

# summary elements

@app.callback(
    Output('summary', 'children'),
    Input('device_dropdown', 'value'),
    Input('date-range', 'start_date'),
    Input('date-range', 'end_date'))
def glance_update(devices_opt, start_date, end_date):
    df_all = query_db(LONG_QUERY)
    df_sensor = df_all[df_all['sensor'] == devices_opt] 

    start_date = dt.datetime.strptime(start_date, '%Y-%m-%d')
    end_date = dt.datetime.strptime(end_date, '%Y-%m-%d')
    filtered_df = df_sensor[(df_sensor['time'].dt.date>=start_date.date()) &
                     (df_sensor['time'].dt.date<=end_date.date())]


    # glance elements
    new_params_list = device_map_update(devices_opt)
    glance_list = ['time', 'sensor']
    for y in new_params_list:
        glance_list.append(y)
    
    df_glance = filtered_df[glance_list]
    df_final = df_glance.iloc[-1]
    df_final = df_final.reset_index(drop=True)

    str = devices_opt
    n=2
    out_string = [''.join(lis) for lis in group(n, str, '')]  
    out_no = [] 

    for a in out_string: 
        out_no.append(a)
    
    # universal time track for glance realtime elements

    time_now = filtered_df[glance_list[0]].iloc[-1].date()

    # Soil sensor

    if out_no[0] == '00' and out_no[1] == '13':
        soil_ec_max = filtered_df[glance_list[2]].max()
        soil_ec_min = filtered_df[glance_list[2]].min()
        soil_ec_mean = round(filtered_df[glance_list[2]].mean(), 2)

        soil_temp_max = filtered_df[glance_list[3]].max()
        soil_temp_min = filtered_df[glance_list[3]].min()
        soil_temp_mean = round(filtered_df[glance_list[3]].mean(), 2)

        soil_moist_max = filtered_df[glance_list[4]].max()
        soil_moist_min = filtered_df[glance_list[4]].min()
        soil_moist_mean = round(filtered_df[glance_list[4]].mean(), 2)
        
        bat_level = filtered_df['bat_voltage'].iloc[-1]
        bat_level = filtered_df.loc[filtered_df['dev_eui'] == devices_opt, 'bat_voltage'].iloc[-1]

        return html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Soil EC"))),
                            dbc.Row(dbc.CardImg(src="./static/Soil_EC.png")),
                            html.Hr(),
                      
                            dbc.Row(dbc.Col('Max  : {} S/m'.format(soil_ec_max))),
                            dbc.Row(dbc.Col('Min  : {} S/m'.format(soil_ec_min))),
                            dbc.Row(dbc.Col('Mean : {} S/m'.format(soil_ec_mean)))],
                        width=4),
                        html.Hr(),
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Soil Temperature"))),
                            dbc.Row(dbc.CardImg(src="./static/Soil_Temp.png")),
                            html.Hr(),
                        
                            dbc.Row(dbc.Col('Max : {} °C'.format(soil_temp_max))),
                            dbc.Row(dbc.Col('Min : {} °C'.format(soil_temp_min))),
                            dbc.Row(dbc.Col('Mean : {} °C'.format(soil_temp_mean)))], 
                            width=4),
                        html.Hr(),
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Soil Moisture"))),
                            dbc.Row(dbc.CardImg(src="./static/Soil_Moisture.png")),
                            html.Hr(),
                    
                            dbc.Row(dbc.Col('Max : {} %'.format(soil_moist_max))),
                            dbc.Row(dbc.Col('Min : {} %'.format(soil_moist_min))),
                            dbc.Row(dbc.Col('Mean : {} %'.format(soil_moist_mean))),], 
                            width=4),
                        html.Hr(),
                    ])         
            ])

    # Light sensor

    elif out_no[0] == '00' and out_no[1] == '14':
        light_level_max = filtered_df[glance_list[2]].max()
        light_level_min = filtered_df[glance_list[2]].min()
        light_level_mean = round(filtered_df[glance_list[2]].mean(), 2)

        return html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Light Intensity"))),
                            dbc.Row(dbc.CardImg(src="./static/Light_Intensity.png")),
                            html.Hr(),
                      
                            dbc.Row(dbc.Col('Max  : {} lux'.format(light_level_max))),
                            dbc.Row(dbc.Col('Min  : {} lux'.format(light_level_min))),
                            dbc.Row(dbc.Col('Mean : {} lix'.format(light_level_mean)))],
                        width=4),
                    html.Hr(),
                    ]),        
            ])

    # Environmental sensor

    elif out_no[0] == '00' and out_no[1] == '15':
        amb_temp_max = filtered_df[glance_list[2]].max()
        amb_temp_min = filtered_df[glance_list[2]].min()
        amb_temp_mean = round(filtered_df[glance_list[2]].mean(), 2)

        amb_humi_max = filtered_df[glance_list[3]].max()
        amb_humi_min = filtered_df[glance_list[3]].min()
        amb_humi_mean = round(filtered_df[glance_list[3]].mean(), 2)

        return html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Ambient Temperature"))),
                            dbc.Row(dbc.CardImg(src="./static/Ambient_Temp.png")),
                      
                            dbc.Row(dbc.Col('Max  : {} °C'.format(amb_temp_max))),
                            dbc.Row(dbc.Col('Min  : {} °C'.format(amb_temp_min))),
                            dbc.Row(dbc.Col('Mean : {} °C'.format(amb_temp_mean)))],
                        width=5),
                        html.Hr(),
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Ambient Humidity"))),
                            dbc.Row(dbc.CardImg(src="./static/Ambient_Humid.png")),
                          
                            dbc.Row(dbc.Col('Max : {} %'.format(amb_humi_max))),
                            dbc.Row(dbc.Col('Min : {} %'.format(amb_humi_min))),
                            dbc.Row(dbc.Col('Mean : {} %'.format(amb_humi_mean)))], 
                            width=4),
                        html.Hr(),
                ]),        
            ])

    # Soil pH sensor

    elif out_no[0] == '00' and out_no[1] == '16':
        soil_pH_max = filtered_df[glance_list[2]].max()
        soil_pH_min = filtered_df[glance_list[2]].min()
        soil_pH_mean = round(filtered_df[glance_list[2]].mean(), 2)
        return html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Soil pH"))),
                            dbc.Row(dbc.CardImg(src="./static/Soil_pH.png")),
                            html.Hr(),
                      
                            dbc.Row(dbc.Col('Max  : {} '.format(soil_pH_max))),
                            dbc.Row(dbc.Col('Min  : {} '.format(soil_pH_min))),
                            dbc.Row(dbc.Col('Mean : {} '.format(soil_pH_mean)))],
                        width=4),
                    html.Hr(),
                ])
            ])

    # Soil pH sensor

    elif out_no[0] == '70' and out_no[1] == 'B3':
        water_vol_max = filtered_df[glance_list[2]].max()
        water_vol_min = filtered_df[glance_list[2]].min()
        water_vol_mean = round(filtered_df[glance_list[2]].mean(), 2)
        return html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Water Used"))),
                            dbc.Row(dbc.CardImg(src="./static/Smart_Meter.png")),
                      
                            dbc.Row(dbc.Col('Max  : {} Liters'.format(water_vol_max))),
                            dbc.Row(dbc.Col('Min  : {} Liters'.format(water_vol_min))),
                            dbc.Row(dbc.Col('Mean : {} Liters'.format(water_vol_mean)))],
                        width=4),
                    html.Hr(),
                ])
            ])      

    # NPK sensor

    elif out_no[0] == '00' and out_no[1] == '17':
        nitrogen = filtered_df[glance_list[2]].iloc[-1]
        phosphorus = filtered_df[glance_list[3]].iloc[-1]
        potassium = filtered_df[glance_list[4]].iloc[-1]

        nitrogen_max = filtered_df[glance_list[2]].max()
        nitrogen_min = filtered_df[glance_list[2]].min()
        nitrogen_mean = round(filtered_df[glance_list[2]].mean(), 2)

        phosphorus_max = filtered_df[glance_list[3]].max()
        phosphorus_min = filtered_df[glance_list[3]].min()
        phosphorus_mean = round(filtered_df[glance_list[3]].mean(), 2)

        potassium_max = filtered_df[glance_list[4]].max()
        potassium_min = filtered_df[glance_list[4]].min()
        potassium_mean = round(filtered_df[glance_list[4]].mean(), 2)

        return html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Nitrogen"))),
                            dbc.Row(dbc.CardImg(src="./static/Soil_Nitr.png")),
                            html.Hr(),
                      
                            dbc.Row(dbc.Col('Max  : {} %'.format(nitrogen_max))),
                            dbc.Row(dbc.Col('Min  : {} %'.format(nitrogen_min))),
                            dbc.Row(dbc.Col('Mean : {} %'.format(nitrogen_mean)))],
                        width=4),
                        html.Hr(),
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Phosphorus"))),
                            dbc.Row(dbc.CardImg(src="./static/Soil_Phos.png")),
                            html.Hr(),
                        
                            dbc.Row(dbc.Col('Max : {} %'.format(phosphorus_max))),
                            dbc.Row(dbc.Col('Min : {} %'.format(phosphorus_min))),
                            dbc.Row(dbc.Col('Mean : {} %'.format(phosphorus_mean)))], 
                            width=4),
                        html.Hr(),
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Potassium "))),
                            dbc.Row(dbc.CardImg(src="./static/Soil_Pota.png")),
                            html.Hr(),
                        
                            dbc.Row(dbc.Col('Max : {} %'.format(potassium_max))),
                            dbc.Row(dbc.Col('Min : {} %'.format(potassium_min))),
                            dbc.Row(dbc.Col('Mean : {} %'.format(potassium_mean))),], 
                            width=4),
                        html.Hr(),
                ])         
            ])
    else:
        print('no device')

# frames
@app.callback(
    Output('data-frames-update', 'children'),
    Input('frames-range', 'start_date'),
    Input('frames-range', 'end_date')
)
def update_data_frames(start_date, end_date):
    df_wq = query_db(LONG_QUERY)
    df_wq['time'] = pd.DatetimeIndex(df_wq['time']) + dt.timedelta(hours=3)
    df_wq = df_wq.reset_index(drop=True)
    start_date = dt.datetime.strptime(start_date, '%Y-%m-%d')
    end_date = dt.datetime.strptime(end_date, '%Y-%m-%d')
    df_wq = df_wq[(df_wq['time'].dt.date>=start_date.date()) &
                     (df_wq['time'].dt.date<=end_date.date())]

    df_greenhspec = user_info()[user_info()['greenhouse'] == 'batian']
    device_list = df_greenhspec['device_eui'].tolist()
    new_greenhouse_df = df_wq[df_wq['dev_eui'].isin(device_list)]
    new_greenhouse_df = new_greenhouse_df.reset_index(drop=True)
    df = new_greenhouse_df.drop(['ack','ant', 'bat', 'bat_voltage',
       'command', 'data', 'data_rate', 'dev_eui', 'epoch_time', 'frequency', 'gweui', 'lat','lon', 'port','rssi','toa','ts','seqno','snr'], axis=1)
    df_k = df.dropna(how='all', axis=1)
    return generate_table(df_k)

# water meters setting

@app.callback(
    Output('water_min', 'children'),
    Output('water_max', 'children'),
    Input('submit-val', 'n_clicks'),
    State('water_vol_min', 'value'),
    State('water_vol_max', 'value'),
    State('green_dropdown_water', 'value'),
    State('water_vol', 'value'))
def update_water_set(n_clicks, minim, maxim, greenh, parameter): 
    
    db_client.switch_database('settings_db') # switch to agriculture database
    d_time = dt.datetime.now()
    influxdb_entry = {}
    influxdb_entry['time'] = d_time
    
    fields = {}
    fields['user'] = user_em
    fields['parameter'] = parameter
    fields['green_house'] = greenh
    fields['water_vol_min'] = minim
    fields['water_vol_max'] = maxim

    influxdb_entry['fields'] = fields
    influxdb_entry['measurement'] = 'settings_data'
    influxdb_entry['tags'] = {'user': user_em}
    db_client.write_points([influxdb_entry])
    return minim, maxim

# set the content according to the current pathname

@app.callback(Output("page-content", "children"), Input("url", "pathname"))
def render_page_content(pathname):

    if pathname == "/settings":
        return html.Div(
            [
                dbc.ModalHeader("Settings"),
                dbc.Row(
                    [
                        dbc.Col(html.Div("Parameter")),
                        dbc.Col(html.Div("Greenhouse")),
                        dbc.Col(html.Div("Minimum")),
                        dbc.Col(html.Div("Maximum")),
                        dbc.Col(html.Div("")),
                    ]
                ),
                html.P(),
                dbc.Row(
                    [
                        dbc.Col(
                            dcc.Dropdown(
                                id='water_vol',
                                options=[
                                    {'label': 'Soil EC', 'value': 'soil_ec'},
                                    {'label': 'Soil Temperature', 'value': 'soil_temp'},
                                    {'label': 'Soil Moisture', 'value': 'soil_moist'},
                                    {'label': 'Ambient Temperature', 'value': 'amb_temp'},
                                    {'label': 'Ambient Humidity', 'value': 'amb_humi'},
                                    {'label': 'Soil pH', 'value': 'soil_pH'},
                                    {'label': 'Light Intensity', 'value': 'light_intensity'},
                                    {'label': 'Water Volume', 'value': 'water_vol'}
                                ],
                                style= {'color':'black'},
                                value='water_vol'),
                        ),
                        dbc.Col(
                            dcc.Dropdown(
                                id='green_dropdown_water',
                                options=[{'label': j, 'value': j} for j in dev_per_greenh().keys()],
                                value= list(dev_per_greenh().keys())[0],
                                style= {'color':'black'}
                        )),
                        dbc.Col([
                            html.Div(dcc.Input(id='water_vol_min', type='number')),
                            html.Div(id='water_min')]),
                        dbc.Col([
                            html.Div(dcc.Input(id='water_vol_max', type='number')),
                            html.Div(id='water_max')]),
                        dbc.Col(html.Button('Submit', id='submit-val', n_clicks=0)),
                    ]
                ),
            ]),
    elif pathname == "/dataframes":
        return html.Div(
            [
                dbc.ModalHeader("Frames"),
                dbc.Row([
                    dbc.Col(
                        html.Div([
                            dcc.DatePickerRange(
                                id='frames-range',
                                min_date_allowed=dt.datetime.now() - dt.timedelta(weeks=8),
                                max_date_allowed=dt.datetime.now(),
                                initial_visible_month=dt.datetime.now(),
                                display_format='MMM Do, YYYY',
                                start_date=(dt.datetime.now() - dt.timedelta(days=1)).date(),
                                end_date=dt.datetime.now().date(), 
                            ),
                ]), 
                width=12, lg=4)]),
                html.Hr(),
                dbc.Row([
                    dbc.Col(
                        html.Div(id= "data-frames-update"), width=6, lg=12)]),
                dcc.Interval(id='refresh-frames-update', interval=DATE_RANGE_UPDATE, n_intervals=0),
            ])
    # If the user tries to reach a different page, return a 404 message
    else:
        return html.Div(
            [
                dbc.Row([
                    dbc.Col(
                        html.Div([
                            dcc.DatePickerRange(
                                id='date-range',
                                min_date_allowed=dt.datetime.now() - dt.timedelta(weeks=8),
                                max_date_allowed=dt.datetime.now(),
                                initial_visible_month=dt.datetime.now(),
                                display_format='MMM Do, YYYY',
                                start_date=(dt.datetime.now() - dt.timedelta(days=1)).date(),
                                end_date=dt.datetime.now().date(), 
                                style= {'color':'black'}
                            ),
                            dcc.Interval(id='range-update', interval=DATE_RANGE_UPDATE, n_intervals=0),
                        ]), 
                    width=12, lg=4)]),
                html.Hr(),
                dbc.Row(
                    [
                        dbc.Col(
                            html.Div([
                                dcc.Dropdown(
                                    id='green_dropdown',
                                    options=[{'label': j, 'value': j} for j in dev_per_greenh().keys()],
                                    value= list(dev_per_greenh().keys())[0],
                                    style= {'color':'black'}
                                )]),
                            width=6, lg=4),
                        dbc.Col(
                            html.Div([
                                dcc.Dropdown(
                                id='device_dropdown',
                                style= {'color':'black'}
                            )]),
                            width=6, lg=4),
                        dbc.Col(
                            html.Div([
                                dcc.Dropdown(
                                    id='parameter_dropdown',
                                    style= {'color':'black'}
                                )]),
                            width=6, lg=4),
                    ]),
                html.Hr(),
                dbc.Row(
                    [
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Parameter Analysis")), justify='center'),
                            dcc.Graph(id='plot', config= {'displaylogo': False})],
                            lg=5),
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Sensor Location")), justify='center'),
                            dcc.Graph(id= 'map', config= {'displaylogo': False})], 
                            lg=4),
                        dbc.Col([
                            dbc.Row(dbc.Col(html.Div("Latest Updates")), justify='center'),
                            html.Div(id='glance')], 
                            lg=3),
                    ]),
                html.Hr(),
                dbc.Row(dbc.Col(html.Div("Summary Report"))),
                html.Hr(),
                dbc.Row([
                    dbc.Col([
                        html.Div(id= 'summary')
                    ], lg=5),
                    dbc.Col([
                        html.Div(id= 'analysis')
                    ], lg=7),
                ]),
                html.Hr(),
            ])

if __name__ == "__main__":
    application.run(port=8080, debug=True)